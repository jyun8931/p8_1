import java.util.Scanner;

/**
 * Represent the ComboLock
 * @author John Yun
 * @author john.yun@sjsu.edu
 * @version 1.0
 *
 * Description: The combo lock class is the definition
 *              of a lock that has secret number values
 *              that when opened in the correct order
 *              it opens the lock successfully
 */
public class ComboLock {
    // private fields definitions
    private Boolean boolOpenedFlag;
    private int iCurrentTick;
    private int[] arrISecrets = new int[3];

    //Default Constructor
    public ComboLock(int secret1, int secret2, int secret3){
        boolOpenedFlag = false;
        iCurrentTick = 0;
        arrISecrets[0] = secret1;
        arrISecrets[1] = secret2;
        arrISecrets[2] = secret3;
    }

    //Getter methods
    public Boolean getiOpenedFlag() {
        return boolOpenedFlag;
    }
    public int getiCurrentTick() {
        return iCurrentTick;
    }
    //we will not write any setters because we dont want
    //the user to be able to change the ticks through a
    //setter. Instead we will use other implementation
    //to set the ticks and the flag.

    /**
     * This function will reset the lock for another try
     * to take place. sets dial to 0
     */
    public void reset(){
        iCurrentTick = 0;
    }

    /**
     * This method turns the dial to the left a certain amount of ticks
     * and prints out the current number
     * @param ticks
     */
    public void turnLeft(int ticks){
        iCurrentTick += ticks;
        if(iCurrentTick > 39){
            iCurrentTick %= 40;
        }

        System.out.printf("You are at dial #: %d\n", iCurrentTick);
    }

    /**
     * This method turns the dial to the right a certain amount of ticks
     * and prints out the current number
     * @param ticks
     */
    public void turnRight(int ticks){
        iCurrentTick -= ticks;
        if(iCurrentTick < 0) {
            iCurrentTick %= 40;
            iCurrentTick = 40 + iCurrentTick;
        }
        System.out.printf("You are at dial #: %d\n", iCurrentTick);
    }

    /**
     * This method gives the user an attempt to open up the lock.
     * @return true if the lock was opened and false if not
     */
    public boolean open(){
        boolOpenedFlag = false;
        int atmpt1, atmpt2, atmpt3;
        Scanner user = new Scanner(System.in);
        System.out.print("******************\n");
        System.out.print( "- Attempt to open -\n");

        //first input
        System.out.print(" Enter first amount of ticks to the right: ");
        atmpt1 = user.nextInt();
        turnRight(atmpt1);
        atmpt1 = getiCurrentTick();

        //second input
        System.out.print("\nEnter second amount of ticks to the left: ");
        atmpt2 = user.nextInt();
        turnLeft(atmpt2);
        atmpt2 = getiCurrentTick();

        //third input
        System.out.print("\nEnter third amount of ticks to the right: ");
        atmpt3 = user.nextInt();
        turnRight(atmpt3);
        atmpt3 = getiCurrentTick();

        if(arrISecrets[0] == atmpt1){
            if(arrISecrets[1] == atmpt2)
                if(arrISecrets[2] == atmpt3)
                    boolOpenedFlag = true;
        }
        reset();
        return boolOpenedFlag;
    }
}
