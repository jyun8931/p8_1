/**
 * This is the driver class for our ComboLock implementation
 * here we will ask the user to try to attempt to open the
 * lock. If failed the user will be asked if he/she wants to
 * try again. Loop will continue until user wants to stop.
 *
 * @author John Yun
 * @author john.yun@sjsu.edu
 * @version 1.0
 * Date: 9/23/2021
 *
 */

import java.util.Scanner;

public class P8_1 {
    public static void main(String[] args) {
        ComboLock comGymLock = new ComboLock(10, 15, 30);
        Scanner keyboard = new Scanner(System.in);
        String input;
        do{
            if(comGymLock.open()){
                System.out.println("Successfully opened the lock!\n");
                break;
            }
            else System.out.println("Failed to open the lock. Try again? [Y|N]");
            input = keyboard.next();
        }while(input.compareTo("Y") == 0);

        System.out.println("Thank you for using the Gym lock! See you next time!\n");

    }
}
/** SAMPlE OUTPUT ***********************************************************
 *
 * ******************
 * - Attempt to open -
 *  Enter first amount of ticks to the right: 30
 * You are at dial #: 10
 *
 * Enter second amount of ticks to the left: 10
 * You are at dial #: 20
 *
 * Enter third amount of ticks to the right: 25
 * You are at dial #: 35
 * Failed to open the lock. Try again? [Y|N]
 * Y
 * ******************
 * - Attempt to open -
 *  Enter first amount of ticks to the right: 30
 * You are at dial #: 10
 *
 * Enter second amount of ticks to the left: 5
 * You are at dial #: 15
 *
 * Enter third amount of ticks to the right: 25
 * You are at dial #: 30
 * Successfully opened the lock!
 *
 * Thank you for using the Gym lock! See you next time!
 *
 *
 * Process finished with exit code 0
 *******************************************************************************/
